import numpy as np
import time


def MatrixMultiply(m1, m2, m3):
    """
    matrix multiplication
    :param m1: matrix
    :param m2: matrix
    :param m3: result matrix
    """
    for i in range(m1.shape[0]):
        for j in range(m2.shape[1]):
            for k in range(m1.shape[1]):
                m3[i][j] += m1[i][k] * m2[k][j]

if __name__ == "__main__":
    cost = 0
    for _ in range(10):
        arr1 = 10 * np.random.randn(200, 100)
        arr2 = 10 * np.random.randn(100, 300)
        r_arr = np.zeros((200, 300), dtype='f8')

        start = time.time()
        MatrixMultiply(arr1, arr2, r_arr)
        cost += time.time() - start
    print('Time per request: ', cost / 10)